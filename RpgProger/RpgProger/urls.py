"""
Definition of urls for RpgProger.
"""

from django.urls import path
from django.views.generic import RedirectView
from django.conf import settings
from django.conf.urls.static import static
from django.conf.urls import url

# Uncomment the next two lines to enable the admin:
from django.contrib import admin
admin.autodiscover()

from schedules.views import index

urlpatterns = [
    # Examples:
    # url(r'^$', RpgProger.views.home, name='home'),
    # url(r'^RpgProger/', include('RpgProger.RpgProger.urls')),

    # Uncomment the admin/doc line below to enable admin documentation:
    # url(r'^admin/doc/', include('django.contrib.admindocs.urls')),

    url(r'^admin', admin.site.urls),
    url('', index),
    #path('', RedirectView.as_view(url='/schedules/', permanent=True)),
] + static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)
