from django.db import models

# Create your models here.
class Person(models.Model):
    """A entity for define some person."""

    PERSON_TYPE = (
        ('tch', 'Teacher'),
        ('oth', 'Other'),
    )

    # Fields
    Type = models.CharField(
        max_length = 3,
        choices = PERSON_TYPE,
        blank = False,
        null = False,
        default = 'oth',
    )
    firstName = models.CharField(max_length=32, blank = False, null = False)
    lastName = models.CharField(max_length=64, blank = False, null = False)
    middleName = models.CharField(max_length=64, blank = False, null = True)


    # Methods
    def __str__(self):
        """String for representing the Model object."""
        return f'{self.lastName} {self.firstName} {self.middleName}'



class Schedule(models.Model):

    ROTATION_TYPE = (
        ('ew', 'Every week'),
        ('al', 'A week later')
    )

    DAY_OF_WEEK = (
        (1, 'Monday'),
        (2, 'Tuesday'),
        (3, 'Wednesday'),
        (4, 'Thursday'),
        (5, 'Friday'),
        (6, 'Saturday'),
        (7, 'Sunday'),
    )

    #Fields
    periodFrom = models.DateField(null = False)
    periodTo = models.DateField(null = False)
    dayOfWeek = models.IntegerField(null = False, choices = DAY_OF_WEEK)
    rotation = models.CharField(
        max_length = 2, 
        choices = ROTATION_TYPE, 
        blank = False, 
        null = False, 
        default = 'ew'
    )
    time = models.IntegerField(null = False)
    duration = models.IntegerField(null = False)

    # Methods
    def __str__(self):
        return super().__str__()



class Course(models.Model):

    #Fields
    name = models.CharField(max_length = 128)
    teachers = models.ManyToManyField(Person)

    #Methods
    def __str__(self):
        return self.name



class Class(models.Model):

    CLASS_TYPE = (
        ('lec', 'Lecture'),
        ('sem', 'Seminar'),
        ('lab', 'Practice'),
    )

    #Fields
    type = models.CharField(
        max_length = 3, 
        choices = CLASS_TYPE,
        blank = False,
        null = False,
    )
    classRoom = models.CharField(max_length = 16, blank = False, null = False)
    teachers = models.ManyToManyField(Person)
    schedules = models.ManyToManyField(Schedule)
    course = models.ForeignKey(Course, on_delete = models.PROTECT, null = False)

    #Methods
    def __str__(self):
        return f'{self.type} in {self.classRoom}'



class Group(models.Model):

    #Field
    name = models.CharField(max_length = 32, blank = False, null = False)
    courses = models.ManyToManyField(Course)

    #Methods
    def __str__(self):
        return self.name