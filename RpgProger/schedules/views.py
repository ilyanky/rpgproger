from django.shortcuts import render
from schedules.models import Person

def index(request):
    """View function for home page of site."""

    num_persons = Person.objects.all().count()
    #num_instances = BookInstance.objects.all().count()
    
    # Available books (status = 'a')
    #num_instances_available = BookInstance.objects.filter(status__exact='a').count()
    
    # The 'all()' is implied by default.    
    #num_authors = Author.objects.count()
    
    context = {
        'num_persons': num_persons,
        #'num_instances': num_instances,
        #'num_instances_available': num_instances_available,
        #'num_authors': num_authors,
    }

    # Render the HTML template index.html with the data in the context variable
    return render(request, 'index.html', context=context)