from django.contrib import admin
from schedules.models import Person, Schedule, Course
from schedules.models import Class, Group

# Register your models here.
admin.site.register(Person)
admin.site.register(Schedule)
admin.site.register(Course)
admin.site.register(Class)
admin.site.register(Group)
