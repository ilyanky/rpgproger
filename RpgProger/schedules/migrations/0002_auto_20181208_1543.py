# Generated by Django 2.1.4 on 2018-12-08 12:43

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('schedules', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='group',
            name='courses',
            field=models.ManyToManyField(to='schedules.Course'),
        ),
    ]
